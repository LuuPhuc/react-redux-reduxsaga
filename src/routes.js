import React from 'react';
// Constant
import { PATH, PAGES } from './utils/constants';

// Pages
const LoginPage = React.lazy(() => import('./pages/Auth/Login'));
const RegisterPage = React.lazy(() => import('./pages/Auth/Register'));

export const publicPages = [
    {
        path: PATH.AUTH.LOGIN,
        name: PAGES.NAME.AUTH.LOGIN,
        component: LoginPage,
        auth: true,
        exact: true,
    },
    {
        path: PATH.AUTH.REGISTER,
        name: PAGES.NAME.AUTH.REGISTER,
        component: RegisterPage,
        auth: true,
        exact: true,
    },
];

export const privatePages = [];
