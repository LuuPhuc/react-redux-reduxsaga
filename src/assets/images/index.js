// import loginBackground from './auth/auth-login.jpg';
import loginBackground from './auth/auth-login-background.jpg';
// import loginBackground from './auth/auth-login-background1.jpg';

// Icons
import githubIcon from './icons/github.svg';
import googleIcon from './icons/google.svg';
import linkedinIcon from './icons/linkedin.svg';

export const images = {
    login_background: {
        url: loginBackground,
    },
    register_background: {
        url: require('./auth/auth.svg'),
    },
    forget_background: {
        url: require('./auth/forget.svg'),
    },
    not_found_background: {
        url: require('./not-found/img-not-found.svg'),
    },
    default_avatar: {
        url: require('./avatar/df-avatar.jpg'),
    },

    // Icons
    google_icon: {
        src: googleIcon,
    },
    github_icon: {
        src: githubIcon,
    },
    linkedin_icon: {
        src: linkedinIcon,
    },
};

export const imgSizes = {
    icon: '20px',
    avatar: '32px',
    avatarLarge: '48px',
    button: '48px',
};
