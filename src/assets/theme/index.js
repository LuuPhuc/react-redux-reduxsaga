import { createMuiTheme } from '@material-ui/core/styles';
import { colors, textColors } from '../colors';

// Fonts
import AvenirNextBoldTTF from '../fonts/AvenirNext-Bold.ttf';
import AvenirNextDemiBoldTTF from '../fonts/AvenirNext-DemiBold.ttf';
import AvenirNextItalicTTF from '../fonts/AvenirNext-Italic.ttf';
import AvenirNextMediumTTF from '../fonts/AvenirNext-Medium.ttf';
import AvenirNextMediumItalicTTF from '../fonts/AvenirNext-MediumItalic.ttf';
import AvenirNextRegularTTF from '../fonts/AvenirNext-Regular.ttf';

/**
 * Material UI theme
 * https://material-ui.com/customization/palette/
 */
const AvenirNextBold = {
    fontFamily: 'AvenirNextBold',
    src: `url(${AvenirNextBoldTTF}) format("opentype")`,
};

const AvenirNextDemiBold = {
    fontFamily: 'AvenirNextDemiBold',
    src: `url(${AvenirNextDemiBoldTTF}) format("opentype")`,
};

const AvenirNextItalic = {
    fontFamily: 'AvenirNextItalic',
    src: `url(${AvenirNextItalicTTF}) format("opentype")`,
};

const AvenirNextMedium = {
    fontFamily: 'AvenirNextMedium',
    src: `url(${AvenirNextMediumTTF}) format("opentype")`,
};

const AvenirNextMediumItalic = {
    fontFamily: 'AvenirNextMediumItalic',
    src: `url(${AvenirNextMediumItalicTTF}) format("opentype")`,
};

const AvenirNextRegular = {
    fontFamily: 'AvenirNextRegular',
    src: `url(${AvenirNextRegularTTF}) format("opentype")`,
};

const theme = createMuiTheme({
    palette: {
        background: {
            default: 'rgba(250, 250, 250, 1)',
        },
        primary: {
            main: colors.black_gray,
            light: colors.black_gray_light,
            // dark: colors.green_dark,
            contrastText: colors.black,
        },
    },
    status: {
        danger: colors.red_danger,
    },
    typography: {
        fontFamily: 'Avenir Next Medium',
    },
    shadow: {
        base: 'rgba(87, 88, 215, 0.25)',
        baseHover: 'rgba(87, 88, 215, 0.25)',
        primary: 'rgba(0, 0, 0, 0.1)',
        blue: 'rgba(104, 132, 242, 0.6)',
        purple: 'rgba(194, 107, 212, 0.6)',
        green: 'rgba(40, 190, 55, 0.6)',
        moss: 'rgba(83, 131, 51, 0.6)',
        coral: 'rgba(252, 137, 89, 0.6)',
        red: 'rgba(210, 77, 87, 0.6)',
        gray_base: 'rgb(211, 213, 216)',
    },

    border_gray: `1px solid ${colors.gray['300']}`,
    border_error: `1px solid ${textColors.error}`,

    MuiCssBaseline: {
        '@global': {
            body: {
                WebkitFontSmoothing: 'antialiased',
                MozOsxFontSmoothing: 'grayscale',
                backgroundColor: '#fff',
            },
            '@font-face': [
                AvenirNextBold,
                AvenirNextDemiBold,
                AvenirNextItalic,
                AvenirNextMedium,
                AvenirNextMediumItalic,
                AvenirNextRegular,
            ],
        },
    },
});

export default theme;