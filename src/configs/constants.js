/**
 * App name
 */
export const APP_NAME = process.env.REACT_APP_NAME || 'LuuPhuc Production';

/**
 * App version
 */
export const APP_VERSION = process.env.REACT_APP_VERSION || 'v1.0.0';

/**
 * API URL
 */
export const API_URL = process.env.REACT_APP_API_URL || 'http://127.0.0.1:5000/v1';

// /**
//  * STATE_LOGIN_AUTH
//  */
// export const STATE_LOGIN_AUTH = 'security_token%3D138r5719ru3e1%26url%3Dhttps%3A%2F%2Foauth2.example.com%2Ftoken';

// // GOOGLE
// export const GOOGLE_AUTHORIZATION_URL = 'https://accounts.google.com/o/oauth2/v2/auth';
// export const GOOGLE_TOKEN_URL = 'https://cors-anywhere.herokuapp.com/https://oauth2.googleapis.com/token';

// // LINKED IN
// export const LINKED_IN_AUTHORIZATION_URL = 'https://www.linkedin.com/oauth/v2/authorization';
// export const LINKED_IN_TOKEN_URL = 'https://cors-anywhere.herokuapp.com/https://www.linkedin.com/oauth/v2/accessToken';

// // GITHUB
// export const GITHUB_AUTHORIZATION_URL = 'https://github.com/login/oauth/authorize';
// export const GITHUB_TOKEN_URL = 'https://cors-anywhere.herokuapp.com/https://github.com/login/oauth/access_token';
