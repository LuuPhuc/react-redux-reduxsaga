import localforage from 'localforage';

import { APP_NAME } from './constants';

localforage.config({
  driver: localforage.LOCALSTORAGE,
  name: APP_NAME,
  version: 1.0,
  size: 4980736, // Size of database, in bytes
  storeName: 'app_db', // Should be alphanumeric, with underscores.
  description: 'Local database',
});

export default localforage;
