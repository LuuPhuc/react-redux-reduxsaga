// import './i18n';

// Material
import { CssBaseline } from '@material-ui/core';
import { ThemeProvider } from '@material-ui/core/styles';

// React
import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { PersistGate } from 'redux-persist/integration/react';
import { ConnectedRouter } from 'connected-react-router';

import App from './App';
import configureStore from './redux/store';
import history from './configs/history';
import theme from './assets/theme';
import * as serviceWorker from './utils/serviceWorker';

const store = configureStore({});

ReactDOM.render(
    <Provider store={store}>
        <ThemeProvider theme={theme}>
            <ConnectedRouter history={history}>
                <PersistGate loading={null} persistor={store.__PERSISTOR}>
                    <CssBaseline />
                    <App />
                </PersistGate>
            </ConnectedRouter>
        </ThemeProvider>
    </Provider>,
    document.getElementById('root'),
);

serviceWorker.unregister();
