// React
import React from 'react';
import { useDispatch } from 'react-redux';

// Action
import { userLogin } from '../../redux/actions/user';

import LoginForm from '../../components/forms/LoginForm';

export const LoginFormContainer = ({ children, location, ...props }) => {
    const dispatch = useDispatch();
    // const messageError = useSelector(selectors.messageError());

    console.log('props____', props);
    const handleSubmit = async (values) => {
        console.log('values____', values);
        dispatch(userLogin(values));
        setTimeout(() => {
            props.history.push('/');
        }, 500);
    };

    return <LoginForm onSubmit={handleSubmit} {...props} />;
};
