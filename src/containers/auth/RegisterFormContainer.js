// React
import React from 'react';
import { useDispatch } from 'react-redux';

// Action
import { userRegister } from '../../redux/actions/user';

import RegisterForm from '../../components/forms/RegisterForm';

export const RegisterFormContainer = ({ children, location, ...props }) => {
    const dispatch = useDispatch();
    // const messageError = useSelector(selectors.messageError());

    const handleSubmit = async (values) => {
        dispatch(userRegister(values));
        setTimeout(() => {
            props.history.push('/');
        }, 500);
    };

    return <RegisterForm onSubmit={handleSubmit} {...props} />;
};
