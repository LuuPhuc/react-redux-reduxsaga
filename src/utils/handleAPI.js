import axios from 'axios';

// Constants
import { METHODS } from '../utils/constants';

import * as _ from 'lodash';
axios.defaults.baseURL = process.env.REACT_APP_BASE_URL_API;

const request = [METHODS.GET, METHODS.POST, METHODS.PUT, METHODS.DELETE].reduce((result, method) => {
    result[method] = async (url, data = {}, configs = {}, organizationId) => {
        const accessToken = window.localStorage.getItem('accessToken');
        if (accessToken) configs.headers.Authorization = `Bearer ${accessToken}`;
        if (_.isEqual(method, METHODS.GET)) configs.params = data;
        if (organizationId) configs.headers.organization_id = organizationId;
        try {
            const response = await axios({
                method,
                url,
                data: data.payload,
                ...configs,
            });
            return response.data;
        } catch (e) {
            if (e.response) throw e.response.data;
            throw e;
        }
    };
    return result;
}, {});

export default request;
