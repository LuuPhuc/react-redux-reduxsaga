import React, { Suspense } from 'react';
import { Helmet } from 'react-helmet';
import { Route, Switch } from 'react-router-dom';
import PrivateRoute from './routers/privateRoute';

import { APP_NAME } from './configs/constants';
import { publicPages } from './routes';

function App() {
    return (
        <>
            <Helmet defaultTitle={APP_NAME} titleTemplate={`${APP_NAME} | %s`} />
            <Suspense fallback={<div>loading...</div>}>
                <Switch>
                    {publicPages.map((route) =>
                        route.auth ? (
                            <PrivateRoute key={route.path} {...route} />
                        ) : (
                            <Route key={route.path} {...route} />
                        ),
                    )}
                    <Route render={() => <div>404 Not Found!</div>} />
                </Switch>
            </Suspense>
        </>
    );
}

export default App;
