// Theme
import { colors, textColors } from '../../../assets/colors';
import { fonts, fontFamily, sizes, radius } from '../../../assets/fonts';
// import { theme } from '../../../assets/theme';

export const styles = (theme) => ({
    container: {
        width: '100%',
        '& .MuiInput-underline:before': {
            display: 'none',
        },
        '& .MuiInput-underline:after': {
            display: 'none',
        },
        '& .MuiInputBase-input': {
            // Reset to default style
            lineHeight: 0,
            padding: 0,
            height: '1.375em',
        },
        position: 'relative',
        borderRadius: radius.input,
        border: ({ error }) => (error ? theme.border_error : theme.border_gray),
    },
    root: {
        padding: '0 !important',
    },
    input: {
        width: '100%',
        padding: fonts.tiny,
        fontFamily: fontFamily.medium,
        fontSize: sizes.sm,
        color: textColors.base,
        backgroundColor: colors.gray['50'],
        '&::placeholder': {
            fontSize: fonts.small,
            color: colors.gray_placeholder,
            fontWeight: 500,
        },
    },
    icon: {
        fontSize: sizes.md,
    },
    showPassword: {
        position: 'absolute',
        right: 0,
        top: 0,
        bottom: 0,
        height: '100%',
        display: 'flex',
        alignItems: 'center',
    },
});
