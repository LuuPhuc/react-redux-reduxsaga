// Material
import Grid from '@material-ui/core/Grid';
import IconButton from '@material-ui/core/IconButton';
import VisibilityIcon from '@material-ui/icons/Visibility';
import VisibilityOffIcon from '@material-ui/icons/VisibilityOff';
// import useAutocomplete from '@material-ui/lab/useAutocomplete';
import Input from '@material-ui/core/Input';

// React
import React, { useState } from 'react';
import { useFormContext } from 'react-hook-form';

// Library
import * as _ from 'lodash';
import PropTypes from 'prop-types';

// Constants
import { TYPES } from '../../../utils/constants';

// Styles
import withStyles from '@material-ui/core/styles/withStyles';
import { styles } from './styles';

// Components
const MyInput = ({ id, name, classes, ref, type, label, errors, size, reset, onChange, onBlur, onFocus, ...props }) => {
    console.log('id_____', id);
    // Ref to form
    const { register } = useFormContext();

    const [showPassword, setShowPassword] = useState(false);

    // Handle show password
    const handleShowPassword = () => {
        setShowPassword((prevState) => !prevState);
    };

    return (
        <Grid alignItems="center" className={classes.container} errors={errors} status="error" type={type} container>
            <Input
                inputRef={(el) => {
                    register(el);
                    if (ref && el) {
                        ref.current = el;
                    }
                }}
                id={id}
                name={name}
                className={classes.input}
                type={showPassword || _.isEqual(type, TYPES.SEARCH) ? TYPES.TEXT : type}
                {...props}
            />
            {type && (_.isEqual(type, TYPES.PASSWORD) || _.isEqual(type, TYPES.SEARCH)) && (
                <>
                    <div className={classes.showPassword}>
                        {_.isEqual(type, TYPES.PASSWORD) ? (
                            <IconButton aria-label="delete" onClick={handleShowPassword}>
                                {showPassword ? (
                                    <VisibilityOffIcon classes={{ root: classes.icon }} fontSize="small" />
                                ) : (
                                    <VisibilityIcon classes={{ root: classes.icon }} fontSize="small" />
                                )}
                            </IconButton>
                        ) : (
                            <Grid className={classes.containerIcon} onClick={() => reset()}>
                                {/* <Icon color="secondary" name="close-svg" /> */}
                                abc
                            </Grid>
                        )}
                    </div>
                </>
            )}
        </Grid>
    );
};

MyInput.propTypes = {
    id: PropTypes.string,
    reset: PropTypes.func,
    type: PropTypes.string,
    label: PropTypes.string,
    readOnly: PropTypes.bool,
    errors: PropTypes.string,
    ref: PropTypes.oneOfType([
        // Either a function
        PropTypes.func,
        // Or the instance of a DOM native element (see the note about SSR)
        PropTypes.shape({ current: PropTypes.instanceOf(Element) }),
    ]),
};

MyInput.defaultProps = {
    id: '',
    label: '',
    readOnly: false,
    ref: undefined,
    reset: undefined,
    type: TYPES.TEXT,
};

export default withStyles(styles)(MyInput);
