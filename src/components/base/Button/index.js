// Material
import { Button as MuiButton } from '@material-ui/core';

// React
import React from 'react';
import PropTypes from 'prop-types';

// Styles
import withStyles from '@material-ui/core/styles/withStyles';
import { styles } from './styles';

const Button = React.forwardRef(
    ({ classes, color, fullWidth, size, children, onClick, loading, src, alt, ...props }, ref) => {
        return (
            <MuiButton
                ref={ref}
                classes={{ root: classes.root, label: classes.label }}
                fullWidth={fullWidth}
                onClick={onClick}
                {...props}
            >
                {src ? <img alt={alt} src={src} /> : children}
            </MuiButton>
        );
    },
);

Button.propTypes = {
    onClick: PropTypes.func,
};

export default withStyles(styles)(Button);
