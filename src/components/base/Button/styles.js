// Library
// import { isEqual } from 'lodash';

// Theme
import { colors } from '../../../assets/colors';
import { fonts, fontFamily, radius } from '../../../assets/fonts';

// Constants
// import { sizes } from '../../../assets/fonts';

export const styles = (theme) => ({
    root: {
        display: 'flex',
        boxShadow: fonts.none,
        borderRadius: radius.input,
        textTransform: fonts.none,
        alignItems: fonts.center,
        justifyContent: fonts.center,
        cursor: 'pointer',
        color: colors.white,
        backgroundColor: colors.black,
        height: 'auto',
        fontFamily: fontFamily.demiBold,
        '&:hover': {
            backgroundColor: colors.black_gray_light,
        },
        borderColor: ({ borderColor }) => colors.border_color_rgba,
        // borderColor: colors.gray,
        // backgroundColor: ({backgroundColor, isOutlined}) => isOutlined ? colors.black : colors.,
        // height: ({ size }) => (isEqual(size, sizes.small) ? '40px' : 'auto'),
        // '&:hover': {
        //     backgroundColor: ({ backgroundColor }) => theme.palette[backgroundColor].light,
        //     borderColor: ({ borderColor, isOutlined }) =>
        //       isOutlined ? theme.palette.background.default : theme.palette[borderColor].light,
        //     boxShadow: 'none',
        //   },
    },
    '&:active': {
        boxShadow: fonts.none,
    },
    '&:focus': {
        outline: 'none !important',
    },
    label: {
        fontSize: fonts.large,
        color: colors.white,
        // fontSize: (props) => theme.typography.pxToRem(props.fontSize),
        // lineHeight: (props) => props.lineHeight,
        // fontWeight: 600,

        // '& img': {
        //   width: '25px',
        // },
    },
});
