// Material
import MuiLink from '@material-ui/core/Link';

// React
import React from 'react';
import { Link as LinkRouter } from 'react-router-dom';
import PropTypes from 'prop-types';

// Styles
import withStyles from '@material-ui/core/styles/withStyles';
import { styles } from './styles';

const MyLink = ({ blank, classes, children, to, onClick, ...props }) => {
    const handleOnClick = (e) => {
        e.preventDefault();
        onClick(e);
    };

    return blank ? (
        <MuiLink className={classes.container} {...props}>
            {children}
        </MuiLink>
    ) : (
        <LinkRouter className={classes.container} to={to} onClick={handleOnClick} {...props}>
            {children}
        </LinkRouter>
    );
};

MyLink.propTypes = {
    onClick: PropTypes.func,
};

MyLink.defaultProps = {
    onClick: undefined,
};
export default withStyles(styles)(MyLink);
