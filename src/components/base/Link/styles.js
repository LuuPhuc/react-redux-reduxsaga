import { fontFamily } from '../../../assets/fonts';
import { textColors } from '../../../assets/colors';

export const styles = (theme) => ({
    container: {
        color: textColors.url,
        fontFamily: (props) => (props.bold ? fontFamily.bold : fontFamily.medium),
        cursor: 'pointer',
    },
});
