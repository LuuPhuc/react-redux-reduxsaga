import { textColors } from '../../../assets/colors';
import { fonts, fontFamily } from '../../../assets/fonts';

export const styles = (theme) => ({
    root: {
        color: ({ color }) => `${textColors.base}`,
    },
    header: {
        fontSize: theme.typography.pxToRem(fonts.h3),
        fontFamily: fontFamily.demiBold,
        fontWeight: 600,
    },
    caption: {
        fontSize: theme.typography.pxToRem(fonts.large),
        color: 'rgb(107, 114, 124)',
        fontWeight: 500,
    },
    title: {
        fontSize: fonts.title,
        lineHeight: fonts.lineHeight,
    },
    tiny: {
        fontSize: fonts.tiny,
    },
    superTiny: {
        fontSize: fonts.superTiny,
    },
    subTitle: {
        fontSize: fonts.large,
    },
});
