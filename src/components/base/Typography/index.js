// Material
import TypographyMui from '@material-ui/core/Typography';

// React
import React from 'react';

// Library
import PropTypes from 'prop-types';

// Styles
import withStyles from '@material-ui/core/styles/withStyles';
import { styles } from './styles';

const Typography = ({ classes, variant, children, gutterBottom, align, color, ...props }) => {
    return (
        <TypographyMui className={classes[variant]} {...props}>
            {children}
        </TypographyMui>
    );
};

Typography.propTypes = {
    variant: PropTypes.string,
};

export default withStyles(styles)(Typography);
