import { noop } from 'lodash';
import PropTypes from 'prop-types';
import React from 'react';

const Form = ({ onSubmit, children, ...props }) => {
    return (
        <form onSubmit={onSubmit} {...props}>
            {children}
        </form>
    );
};

Form.propTypes = {
    onSubmit: PropTypes.func,
};

Form.defaultProps = {
    onSubmit: noop,
};

export default Form;
