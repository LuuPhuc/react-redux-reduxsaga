import * as yup from 'yup';

export const loginSchema = yup.object().shape({
    id: yup.string().email().max(128).required('Please fill out this field'),
    password: yup
        .string()
        // .matches(/^(?=.*[a-zA-Z])(?=.*[0-9]).{6,}$/, MESSAGE_VALIDATE_PASSWORD)
        .max(128, 'Password must be maximum 128 characters')
        .required(),
});
