// Library
// import * as _ from 'lodash';
import { yupResolver } from '@hookform/resolvers/yup';
import PropTypes from 'prop-types';

// React
import React from 'react';
import { FormProvider, useForm } from 'react-hook-form';

// Material
import Grid from '@material-ui/core/Grid';

// Components
import Form from '../../base/Form';
import Typography from '../../base/Typography';
import Link from '../../base/Link';
import Input from '../../base/Input';
import Button from '../../base/Button';

// Styles
import withStyles from '@material-ui/core/styles/withStyles';
import { styles } from './styles';

// Images
import { images } from '../../../assets/images';

// Validation
import { loginSchema } from './validation';

const LoginForm = ({ classes, onSubmit, submitting, error, waiting, ...props }) => {
    console.log('error____', error);
    console.log('onSubmit____', onSubmit);
    const methods = useForm({
        resolver: yupResolver(loginSchema),
    });

    const { handleSubmit, errors } = methods;
    console.log('methods______', methods);
    return (
        <FormProvider {...methods}>
            <Form onSubmit={handleSubmit(onSubmit)}>
                <Typography variant="header">Welcome back</Typography>
                <Grid className={classes.containerHorizontal} justify="center" container>
                    <Typography variant="subTitle">New to timebee? </Typography>
                    <Link to="/signup" bold underline>
                        Sign up
                    </Link>
                </Grid>

                {/* Error message */}
                {error && (
                    <Typography className={classes.errorMessage} color="error" component="div">
                        {error}
                    </Typography>
                )}

                <div className={classes.containerEmail}>
                    <Input error={errors.id?.message} id="id" name="email" placeholder="Your email address" />
                </div>
                <div className={classes.containerPassword}>
                    <Input
                        error={errors.password?.message}
                        id="password"
                        name="password"
                        placeholder="Password"
                        type="password"
                    />
                </div>
                <Button className={classes.signIn} color="primary" loading={submitting && error} type="submit">
                    Sign in
                </Button>
                <Grid justify="flex-end" container>
                    <Link color="primary" to="/forgot-password" bold underline>
                        Forgot your password?
                    </Link>
                </Grid>
                <Grid className={classes.captionExtended}>
                    <Typography color="light" variant="caption">
                        Or login with
                    </Typography>
                </Grid>

                {/* Login by oauth2 */}
                <Grid className={classes.groupAuth} justify="space-between" container>
                    <Grid item>
                        <Button
                            variant="outlined"
                            className={classes.oauth2}
                            alt="linked in"
                            src={images.linkedin_icon.src}
                        />
                    </Grid>
                    <Grid item>
                        <Button
                            variant="outlined"
                            className={classes.oauth2}
                            alt="google"
                            src={images.google_icon.src}
                        />
                    </Grid>
                    <Grid item>
                        <Button
                            variant="outlined"
                            className={classes.oauth2}
                            alt="github"
                            src={images.github_icon.src}
                        />
                    </Grid>
                </Grid>
                <Typography className={classes.term} color="light" component="div" variant="small">
                    By continue, you're agreeing to out
                    <Link color="primary" to="/term">
                        Terms of Use
                    </Link>{' '}
                    and
                    <Link color="primary" to="/policy">
                        Privacy policy
                    </Link>
                </Typography>
            </Form>
        </FormProvider>
    );
};

LoginForm.propTypes = {
    onSubmit: PropTypes.func,
    handleAuth: PropTypes.func,
    submitting: PropTypes.bool,
    error: PropTypes.string,
    waiting: PropTypes.bool,
};

LoginForm.defaultProps = {};

export default withStyles(styles)(LoginForm);
