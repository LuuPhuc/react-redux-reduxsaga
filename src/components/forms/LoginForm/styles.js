import { fonts } from '../../../assets/fonts';
import { colors } from '../../../assets/colors';

export const styles = (theme) => ({
    containerHorizontal: {
        display: 'flex',
        margin: '20px 0 40px 0',
        fontSize: fonts.large,
    },
    errorMessage: {
        marginTop: '20px',
        textAlign: 'center',
    },
    containerEmail: {
        width: '100%',
        marginBottom: '30px',
    },
    containerPassword: {
        width: '100%',
    },
    signIn: {
        width: '100%',
        marginTop: '40px',
        padding: '10px 27px',
    },
    captionExtended: {
        textAlign: 'left',
        marginTop: '70px',
    },
    groupAuth: {
        marginTop: '11px',
    },
    oauth2: {
        padding: '18px 37px',
        backgroundColor: colors.white,
    },
    term: {
        marginTop: '40px',
        fontSize: '13px',
        textAlign: 'left',
    },
});
