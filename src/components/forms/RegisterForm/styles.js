import { fonts } from '../../../assets/fonts';
import { colors } from '../../../assets/colors';

export const styles = (theme) => ({
    containerHorizontal: {
        display: 'flex',
        margin: '20px 0 40px 0',
        fontSize: fonts.large,
    },
});
