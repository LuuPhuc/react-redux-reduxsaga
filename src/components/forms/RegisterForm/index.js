// Library
import { yupResolver } from '@hookform/resolvers/yup';
import PropTypes from 'prop-types';

// React
import React from 'react';
import { FormProvider, useForm } from 'react-hook-form';

// Material
import Grid from '@material-ui/core/Grid';

// Components
import Form from '../../base/Form';
import Typography from '../../base/Typography';
import Link from '../../base/Link';
import Input from '../../base/Input';
import Button from '../../base/Button';

// Styles
import withStyles from '@material-ui/core/styles/withStyles';
import { styles } from './styles';

// Validation
import { registerSchema } from './validation';

const RegisterForm = ({ classes, onSubmit, submitting, error, waiting, ...props }) => {
    const methods = useForm({
        resolver: yupResolver(registerSchema),
    });

    const { handleSubmit, errors } = methods;

    return (
        <FormProvider {...methods}>
            <Form onSubmit={handleSubmit(onSubmit)}>
                <Typography variant="header">Be more productive</Typography>
                <Grid className={classes.containerHorizontal} justify="center" container>
                    <Typography variant="subTitle">Already signed up? </Typography>
                    <Link to="/signup" bold underline>
                        Log in
                    </Link>
                </Grid>

                {/* Error message */}
                {error && (
                    <Typography className={classes.errorMessage} color="error" component="div">
                        {error}
                    </Typography>
                )}

                <Grid container>
                    <Grid item>
                        <Input error={errors.first_name?.message} id="id" name="first_name" placeholder="First name" />
                        <Input error={errors.last_name?.message} id="id" name="last_name" placeholder="Last name" />
                    </Grid>
                    <Grid item>
                        <Input error={errors.email?.message} id="email" name="email" placeholder="Your email address" />
                    </Grid>
                    <Grid item>
                        <Input
                            error={errors.password?.message}
                            id="password"
                            name="password"
                            placeholder="Create a password"
                            type="password"
                        />
                    </Grid>
                </Grid>
            </Form>
        </FormProvider>
    );
};

RegisterForm.propTypes = {
    onSubmit: PropTypes.func,
    handleAuth: PropTypes.func,
    submitting: PropTypes.bool,
    error: PropTypes.string,
    waiting: PropTypes.bool,
};

RegisterForm.defaultProps = {};

export default withStyles(styles)(RegisterForm);
