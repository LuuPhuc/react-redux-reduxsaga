export const styles = (theme) => ({
    container: {
        width: '100%',
        '& .MuiGrid-container': {
            padding: 0,
        },
    },
    layout: {
        width: '100%',
        maxWidth: '380px',
        margin: '78px auto',
        textAlign: 'center',
    },
    images: {
        width: '33.33%',
        height: '100vh',
        objectFit: 'cover',
        position: 'fixed',
    },
});
