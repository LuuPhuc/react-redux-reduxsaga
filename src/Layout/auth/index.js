// Material
import Grid from '@material-ui/core/Grid';
// import Hidden from '@material-ui/core/Hidden';

// React
import React from 'react';
// import { Helmet } from 'react-helmet';

// Image
import { images } from '../../assets/images';

// Styles
import withStyles from '@material-ui/core/styles/withStyles';
import { styles } from './styles';

const AuthLayout = ({ className, classes, children, title }) => {
    return (
        <Grid spacing={0} className={classes.container} container>
            <Grid xs={4} item>
                <img alt="background" className={classes.images} src={images.login_background.url} />
            </Grid>
            <Grid className={classes.layout} xs={8} item>
                {children}
            </Grid>
        </Grid>
    );
};

export default withStyles(styles)(AuthLayout);
