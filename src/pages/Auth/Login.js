import AuthLayout from '../../Layout/auth';
import { LoginFormContainer } from '../../containers';
import React from 'react';

const LoginPage = (props) => {
    return (
        <AuthLayout>
            <LoginFormContainer {...props} />
        </AuthLayout>
    );
};

export default LoginPage;
