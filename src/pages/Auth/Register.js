import AuthLayout from '../../Layout/auth';
import { RegisterFormContainer } from '../../containers';
import React from 'react';

const RegisterPage = (props) => {
    return (
        <AuthLayout>
            <RegisterFormContainer {...props} />
        </AuthLayout>
    );
};

export default RegisterPage;
