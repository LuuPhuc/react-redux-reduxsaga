import request from '../../../utils/handleAPI';

export function loginApi(payload) {
    return request.post('/auth/login', payload);
}
