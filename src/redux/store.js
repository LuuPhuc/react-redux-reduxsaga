// Redux
import { applyMiddleware, createStore } from 'redux';
import createSagaMiddleware from 'redux-saga';
import { composeWithDevTools } from 'redux-devtools-extension';
import { routerMiddleware } from 'connected-react-router';

// Reducers
import rootReducer from './reducers';

// Sagas
import rootSaga from '../redux/sagas/user';

// Configs
import localForage from '../configs/localforage';
import { APP_VERSION } from '../configs/constants';
import history from '../configs/history';

import autoMergeLevel2 from 'redux-persist/lib/stateReconciler/autoMergeLevel2';

const configureStore = (preloadedState = {}) => {
    const sagaMiddleware = createSagaMiddleware();

    const middlewares = [routerMiddleware(history), sagaMiddleware];

    const enhancers = [applyMiddleware(...middlewares)];

    const { persistReducer, persistStore } = require('redux-persist');

    const persistConfig = {
        key: `app_${APP_VERSION}`,
        storage: localForage,
        stateReconciler: autoMergeLevel2,
    };

    const persistedReducer = persistReducer(persistConfig, rootReducer);

    const store = createStore(persistedReducer, preloadedState, composeWithDevTools(...enhancers));
    store.__PERSISTOR = persistStore(store);
    // Hot reload reducer
    if (process.env.NODE_ENV !== 'production') {
        if (module.hot) {
            module.hot.accept('./reducers', () =>
                // eslint-disable-next-line global-require
                store.replaceReducer(require('./reducers').default),
            );
        }
    }
    sagaMiddleware.run(rootSaga);
    return store;
};

export default configureStore;
