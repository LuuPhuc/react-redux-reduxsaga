// Saga
import { all, put, takeLatest, call } from 'redux-saga/effects';
// import { toast } from 'react-toastify';

// Action types
import { userActionTypes } from '../../actions/user';

// Apis
import { loginApi } from '../../apis/user';

function* userLogin(action) {
    console.log('action_______', action);
    const params = action;
    try {
        const payload = yield call(loginApi, params);
        yield put({ type: userActionTypes.USER_LOGIN_SUCCESS, payload });
        yield put({ type: userActionTypes.GET_USER_INFO });
    } catch (error) {
        yield put({ type: userActionTypes.REGISTER_FAILURE, error });
    }
}

export default function* authSaga() {
    yield all([takeLatest(userActionTypes.USER_LOGIN, userLogin)]);
}
