import { userActionTypes } from '../../actions/user';

const defaultState = {
    fetching: {},
    error: null,
    loading: false,
    userInfo: null,
    registerSuccessful: null,
};

const userReducer = (state = defaultState, { type, payload, error }) => {
    console.log('type________', type);
    console.log('payload_____', payload);
    switch (type) {
        case userActionTypes.COMMON_SUCCESS:
            return { ...state, loading: false };
        case userActionTypes.COMMON_FAILED:
            return { ...state, loading: false, error: payload };

        case userActionTypes.USER_LOGIN:
            return {
                ...state,
                loading: true,
                userInfo: null,
            };
        case userActionTypes.USER_LOGIN_SUCCESS:
            return {
                ...state,
                loading: false,
                userInfo: payload,
            };

        case userActionTypes.USER_REGISTER:
            return {
                ...state,
                registerFailure: null,
                userInfo: null,
            };
        case userActionTypes.USER_REGISTER_SUCCESS:
            const { tokens, user } = payload;
            localStorage.setItem('access_token', tokens.access_token);
            return {
                ...state,
                userInfo: user,
            };
        default:
            return state;
    }
};

export default userReducer;
