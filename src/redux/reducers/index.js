import { combineReducers } from 'redux';
import { connectRouter } from 'connected-react-router';
import history from '../../configs/history';
import userReducer from './user';

console.log('userReducer______', userReducer);
const rootReducer = combineReducers({
    user: userReducer,
    router: connectRouter(history),
});

export default rootReducer;
