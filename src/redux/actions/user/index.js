export const userActionTypes = {
    USER_LOGIN: 'USER_LOGIN',
    USER_LOGIN_SUCCESS: 'USER_LOGIN_SUCCESS',

    USER_REGISTER: 'USER_REGISTER',
    USER_REGISTER_SUCCESS: 'USER_REGISTER_SUCCESS',
    USER_REGISTER_FAILURE: 'USER_REGISTER_FAILURE',

    GET_USER_INFO: 'GET_USER_INFO',

    COMMON_FAILED: 'COMMON_FAILED',
    COMMON_SUCCESS: 'COMMON_SUCCESS',
};

export const userLogin = (payload) => ({
    type: userActionTypes.USER_LOGIN,
    payload,
});

export const userRegister = (payload) => ({
    type: userActionTypes.USER_REGISTER,
    payload,
});

export const getUserInfo = (payload) => ({
    type: userActionTypes.GET_USER_INFO,
    payload,
});
