// Library
import * as _ from 'lodash';
import PropTypes from 'prop-types';

// React
import React from 'react';
import { useSelector, shallowEqual } from 'react-redux';
import { Redirect, Route } from 'react-router-dom';

// Constants
import { PATH } from '../utils/constants';
// Selectors
// import * as selectors from 'store/auth/selectors';
// import * as commonSelectors from 'store/common/selectors';
// import authUtils from '../utils/cookie';

// const { getSession } = authUtils();

const PrivateRoute = ({ components: Components, needOrganization, ...rest }) => {
    // Get token
    // const token = getSession([TYPES.ACCESS_TOKEN]);

    const { isAuthorized } = useSelector(
        ({ user }) => ({
            isAuthorized: user.userInfo != null,
        }),
        shallowEqual,
    );

    // const { hasOrganization } = useSelector((organization) => {});
    if (_.isEqual(rest.path, PATH.AUTH.LOGIN))
        return (
            <Route
                {...rest}
                render={(props) => {
                    !isAuthorized ? <Components {...props} /> : <Redirect to={{ pathname: 'organizations' }} />;
                }}
            ></Route>
        );

    // return (
    //     <Route
    //         {...rest}
    //         render={(props) => {
    //             !isAuthorized ? (
    //                 <Redirect
    //                     to={{
    //                         pathname: PATH.AUTH.LOGIN,
    //                         search: `?from=${rest.location.pathname}`,
    //                     }}
    //                 />
    //             ) : hasOrganization ? (
    //                 <Redirect
    //                     to={{
    //                         pathname: '/choose-your-organization',
    //                     }}
    //                 />
    //             ) : (
    //                 <Components {...props} />
    //             );
    //         }}
    //     ></Route>
    // );
};

PrivateRoute.propTypes = {
    components: PropTypes.elementType.isRequired,
    needOrganization: PropTypes.bool,
};

PrivateRoute.defaultProps = {
    needOrganization: false,
};

export default PrivateRoute;
